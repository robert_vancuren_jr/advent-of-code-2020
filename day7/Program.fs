open System
open System.Text.RegularExpressions

type RequiredContents = { color: string; count: int }

type Rule =
    { color: string
      requiredContents: Option<seq<RequiredContents>> }

let readLines fileName = System.IO.File.ReadLines(fileName)

let getValueFrom (group: GroupCollection) key = group.TryGetValue(key) |> snd |> string

let parseRule (rule: string) =
    let regex = "(?<count>\d+) (?<color>.*) bag|bags"

    let m = Regex.Match(rule, regex)
    let getValueFromMatches = getValueFrom m.Groups

    let color = getValueFromMatches "color"
    let count = getValueFromMatches "count"

    { color = color; count = int count }

let parseRules (rules: string) =
    match rules with
    | "no other bags" -> None
    | _ -> Some(rules.Split "," |> Seq.map parseRule)

let lineToRule (line: string): Rule =
    let regex =
        "(?<color>.*) bags contain (?<rules>.*)\."

    let m = Regex.Match(line, regex)
    let getValueFromMatches = getValueFrom m.Groups

    let color = getValueFromMatches "color"

    let rules =
        getValueFromMatches "rules" |> parseRules

    { color = color
      requiredContents = rules }


[<EntryPoint>]
let main argv =
    let rules =
        readLines "puzzle-input"
        |> Seq.map lineToRule
        |> Seq.toList

    let getRuleByColor color =
        Seq.find (fun x -> x.color = color) rules

    let rec canHold color (rule: Rule) =
        let rec checkReqs color (requirments: RequiredContents list) =
            match requirments with
            | [] -> false
            | head :: [] ->
                if head.count > 0 && head.color = color
                then true
                else canHold color (getRuleByColor head.color)
            | head :: tail ->
                if head.count > 0 && head.color = color
                then true
                else canHold color (getRuleByColor head.color)
                || checkReqs color tail


        match rule.requiredContents with
        | None -> false
        | Some (requirments: RequiredContents seq) -> checkReqs color (Seq.toList requirments)

    let canHoldShiny = Seq.filter (canHold "shiny gold") rules

    printfn "Day 7 part One: Number of bags that can hold shiny: %i" (Seq.length canHoldShiny)


    let rec countBagsNeeded rule =
        let rec countEm (requirements: RequiredContents list) =

            match requirements with
            | [] -> 0
            | head :: [] ->
                head.count
                + (head.count
                   * countBagsNeeded (getRuleByColor head.color))
            | head :: tail ->
                (head.count
                 + head.count
                   * countBagsNeeded (getRuleByColor head.color))
                + countEm tail

        match rule.requiredContents with
        | None -> 0
        | Some (requirments: RequiredContents seq) -> countEm (Seq.toList requirments)


    let shinyGoldRule = getRuleByColor "shiny gold"
    let otherBags = countBagsNeeded shinyGoldRule

    printfn "Part two: other bags needed: %A" otherBags

    0
