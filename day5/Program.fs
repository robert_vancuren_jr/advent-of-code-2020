open System

let readLines fileName = System.IO.File.ReadLines(fileName)

let rows = [ 0 .. 127 ]
let columns = [ 0 .. 7 ]

let takeLower list = List.splitInto 2 list |> List.head
let takeUpper list = List.splitInto 2 list |> List.last
let calculateId row col = row * 8 + col
let addId (row, col) = (row, col, calculateId row col)
let seatId (_, _, id) = id
let stringToCharList str = [ for c in str -> c ]

let handleInstruction instruction acc =
    let rows, cols = acc

    match instruction with
    | 'F' -> (takeLower rows, cols)
    | 'B' -> (takeUpper rows, cols)
    | 'R' -> (rows, takeUpper cols)
    | 'L' -> (rows, takeLower cols)
    | _ -> (rows, cols)

let instructionsToSeat (instructions: list<char>) =

    let rec followInstructions instructions acc =
        match instructions with
        | head :: tail -> followInstructions tail (handleInstruction head acc)
        | [] -> (List.head (fst acc), List.head (snd acc))

    followInstructions instructions (rows, columns)

let rec findMySeat (list: list<(int * int * int)>) =
    match list with
    | [] -> failwith "List of seats is empty"
    | head :: [] -> seatId head
    | head :: tail ->
        let currentId = seatId (head)
        let nextId = seatId (List.head tail)
        if (currentId + 1 <> nextId) then currentId + 1 else findMySeat tail


[<EntryPoint>]
let main argv =

    let partOneAnswer =
        readLines "puzzle-input"
        |> Seq.map stringToCharList
        |> Seq.map instructionsToSeat
        |> Seq.map addId
        |> Seq.sortBy seatId
        |> Seq.last
        |> seatId

    printfn "Part one; highest seat id %A" partOneAnswer

    let partTwoAnswer =
        readLines "test-input"
        |> Seq.map stringToCharList
        |> Seq.map instructionsToSeat
        |> Seq.map addId
        |> Seq.sortBy seatId
        |> Seq.toList
        |> findMySeat

    printfn "Part two: my seat id %i" partTwoAnswer

    0 // return an integer exit code
