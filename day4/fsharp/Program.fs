open System
open System.Text.RegularExpressions

let readLines fileName = System.IO.File.ReadLines(fileName)

let getValueFrom (group: GroupCollection) key = group.TryGetValue(key) |> snd |> string

let partitionByEmptyLine list =
    let rec reduce list acc =
        match list with
        | head :: tail ->
            if (Seq.length head > 0)
            then reduce tail (fst acc, snd acc + " " + head)
            else reduce tail ((fst acc @ [ (snd acc).Trim() ]), "")
        | [] -> (fst acc @ [ (snd acc).Trim() ])

    reduce list ([], "")

let keyValueStringToTuple (keyValueString: string) =
    let keyValuePair = keyValueString.Split ":"
    let key = keyValuePair.[0]
    let value = keyValuePair.[1]
    (key, value)

let requiredFields =
    [ "byr"
      "iyr"
      "eyr"
      "hgt"
      "hcl"
      "ecl"
      "pid"
      //"cid"; lol this is optional
     ]

let hasRequiredFields fields =
    let keys = Array.map fst fields |> Array.toList

    List.forall (fun required -> List.contains required keys) requiredFields

let haveAllRequiredFields (passport: string) =
    passport.Split " "
    |> Array.map keyValueStringToTuple
    |> hasRequiredFields

let partOne lines =
    printfn "Day 4 Part 1"
    let passports = partitionByEmptyLine lines

    let valid =
        List.filter haveAllRequiredFields passports

    printfn "have required fields: %i" (Seq.length valid)

let isLength length str = Seq.length str = length

let parseInt (s: string) = int s

let between min max value = min <= value && value <= max

let verifyBirthYear (value: string) =
    let correctLength = value |> isLength 4
    let inRange = int value |> between 1920 2002

    correctLength && inRange

let verifyIssueYear (value: string) =
    let correctLength = value |> isLength 4
    let inRange = int value |> between 2010 2020

    correctLength && inRange

let verifyExpirationYear (value: string) =
    let correctLength = value |> isLength 4
    let inRange = int value |> between 2020 2030

    correctLength && inRange

let verifyHeight (value: string) =
    let group =
        Regex
            .Match(value, "(?<height>\d+)(?<units>.*)")
            .Groups

    let getVal = getValueFrom group
    let height = int (getVal "height")
    let units = getVal "units"

    match units with
    | "cm" -> height |> between 150 193
    | "in" -> height |> between 59 76
    | _ -> false

let verifyHairColor (value: string) =
    Regex.Match(value, "#[0-9a-f]{6}").Success

let acceptableEyeColors =
    [ "amb"
      "blu"
      "brn"
      "gry"
      "grn"
      "hzl"
      "oth" ]

let verifyEyeColor (value: string) = List.contains value acceptableEyeColors

let verifyPassportId (value: string) =
    Seq.length value = 9 && fst (Int32.TryParse value)

let isValid field =
    match field with
    | ("byr", value) -> verifyBirthYear value
    | ("iyr", value) -> verifyIssueYear value
    | ("eyr", value) -> verifyExpirationYear value
    | ("hgt", value) -> verifyHeight value
    | ("hcl", value) -> verifyHairColor value
    | ("ecl", value) -> verifyEyeColor value
    | ("pid", value) -> verifyPassportId value
    //"cid"; lol this is optional
    | _ -> true // don't care about unknown keys

let allFieldsAreValid fields = List.forall isValid fields

let passportIsValid (passport: string) =

    passport.Split " "
    |> Array.map keyValueStringToTuple
    |> Array.toList
    |> allFieldsAreValid


let partTwo lines =
    printfn "Day 4 Part 2"
    let passports = partitionByEmptyLine lines

    let seemsLegit =
        List.filter haveAllRequiredFields passports

    let valid = List.filter passportIsValid seemsLegit

    printfn "valid passport count: %i" (Seq.length valid)


[<EntryPoint>]
let main argv =
    let lines = readLines "puzzle-input" |> Seq.toList

    partOne lines
    partTwo lines


    0 // return an integer exit code
