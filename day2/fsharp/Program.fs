open System
open System.Text.RegularExpressions

type ruleAndPassword = ((int * int) * char) * string

let getValueFrom (group: GroupCollection) key = group.TryGetValue(key) |> snd |> string

let parseLine2 (line: string) =
    let group =
        Regex
            .Match(line, "(?<min>\d+)-(?<max>\d+) (?<char>\w): (?<password>.*$)")
            .Groups

    let getVal = getValueFrom group

    ((int (getVal "min"), int (getVal "max")), char (getVal "char")), getVal "password"


let parseData (data: seq<string>) = (Seq.map parseLine2 data) |> Seq.toList

let loadData fileName = System.IO.File.ReadLines(fileName)

let loadRuleAndPasswords fileName: List<ruleAndPassword> = loadData fileName |> parseData


let isValidSledRentalPlaceDownTheStreetPassword ((((min, max), char), password): ruleAndPassword) =
    let count =
        password
        |> Seq.filter (fun x -> x = char)
        |> Seq.length

    count >= min && count <= max

let isValidNorthPoleTobogganRentalShop ((((posOne, posTwo), char), password): ruleAndPassword) =
    (password.[posOne - 1] = char
     && password.[posTwo - 1] <> char)
    || (password.[posOne - 1] <> char
        && password.[posTwo - 1] = char)

let findPasswords partName filterFun =
    printfn ""
    printfn "Part %s" partName

    let listOfRuleAndPasswords = loadRuleAndPasswords "raw-input"

    let numberOfGoodPasswords =
        List.filter filterFun listOfRuleAndPasswords
        |> Seq.length

    printfn "number of good passwords %i" numberOfGoodPasswords




[<EntryPoint>]
let main argv =

    findPasswords "one" isValidSledRentalPlaceDownTheStreetPassword
    findPasswords "two" isValidNorthPoleTobogganRentalShop

    0 // return an integer exit code
