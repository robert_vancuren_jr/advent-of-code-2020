open System

let readLines fileName = System.IO.File.ReadLines(fileName)
type Board = string []

let checkForTree (board: Board) (x, y) =
    let row = board.[y]
    let column = x % Seq.length row
    let square = row.[column]

    match square with
    | '#' -> 1
    | _ -> 0



let rec toboggan (board: Board) (startX, startY) (velocityX, velocityY) hitCount =
    if (startY >= (Seq.length board) - 1) then
        hitCount
    else
        let newX = startX + velocityX
        let newY = startY + velocityY
        let hit = checkForTree board (newX, newY)

        toboggan board (startX + velocityX, startY + velocityY) (velocityX, velocityY) hitCount
        + hit

let partOne board =
    let treesHit = toboggan board (0, 0) (3, 1) 0

    printfn "Day 3 Part 1"
    printfn "trees hit %i" treesHit
    board

let slopeToHits board (slopeX, slopeY) =
    toboggan board (0, 0) (slopeX, slopeY) 0

let partTwo board =
    printfn "Day 3 Part 2"

    let slopes =
        [ (1, 1)
          (3, 1)
          (5, 1)
          (7, 1)
          (1, 2) ]

    let answer =
        List.map (slopeToHits board) slopes
        |> List.reduce (*)

    printfn "Answer %i" answer

[<EntryPoint>]
let main argv =
    readLines "raw-input"
    |> Seq.toArray
    |> partOne
    |> partTwo


    0 // return an integer exit code
