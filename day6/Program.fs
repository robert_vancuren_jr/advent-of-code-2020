open System

let readLines fileName = System.IO.File.ReadLines(fileName)

let groupByEmptyLine list =
    let rec reduce list acc =
        match list with
        | head :: tail ->
            if (Seq.length head > 0)
            then reduce tail (fst acc, snd acc + " " + head)
            else reduce tail ((fst acc @ [ (snd acc).Trim() ]), "")
        | [] -> (fst acc @ [ (snd acc).Trim() ])

    reduce list ([], "")

let trim (s: string) = s.Trim()
let removeWhiteSpace (s: string) = s.Replace(" ", "")

let removeDuplicates (s: string) =
    let chars = s.ToCharArray()
    Array.distinct chars |> Array.toList

let splitOnSpace (s: string) = s.Split " " |> Array.toList

let everyoneAnsweredYes (currentAnswer: char) (otherAnswer: string) = otherAnswer.Contains currentAnswer

let didEveryoneElseAnswerYes (everyoneOneElsesAnswers: list<string>) (singleAnswer: char) =
    (singleAnswer, List.forall (everyoneAnsweredYes singleAnswer) everyoneOneElsesAnswers)


let answersMatch (groupOfAnswers: list<string>) (currentAnswers: string) =
    currentAnswers.ToCharArray()
    |> Array.toList
    |> List.map (didEveryoneElseAnswerYes groupOfAnswers)

let answersThatEveryoneHad groupOfAnswers =
    List.map (answersMatch groupOfAnswers) groupOfAnswers

let countAnswers list =
    List.map (fun (_, x) -> if x = true then 1 else 0) list
    |> List.sum

let isTrue x = x = true


[<EntryPoint>]
let main argv =
    readLines "puzzle-input"
    |> Seq.toList
    |> groupByEmptyLine
    |> List.map removeWhiteSpace
    |> List.map removeDuplicates
    |> List.map List.length
    |> List.sum
    |> printfn "Part One: Sum of counts %A"

    readLines "puzzle-input"
    |> Seq.toList
    |> groupByEmptyLine
    |> List.map splitOnSpace
    |> List.map answersThatEveryoneHad
    |> List.map List.concat
    |> List.map List.distinct
    |> List.map countAnswers
    |> List.sum
    |> printfn "Part Two: Sum of counts %A"

    0 // return an integer exit code
