const fs = require('fs');

const init = () => {
    fs.readFile('entries.json', 'utf8', readFileHandler);
}

const readFileHandler = (error, data) => {
    if (error) {
        throw "unable to read input file";
    }

    fixExpenseReport(JSON.parse(data));
}

const fixExpenseReport = (entries) => {
    console.info("");
    console.info("Part one");

    const [one, two] = entries.filter(numbersThatSumTo2020);
    console.info("Entries that sum to 2020:", [one, two]);

    const result = one * two;
    console.info("Answer:", result);
}

const numbersThatSumTo2020 = (current, index, entries) => 
    entries.some(sumsTo2020(current))

const sumsTo2020 = a => b => a + b == 2020;

init();
