const fs = require('fs');

const init = () => {
    fs.readFile('entries.json', 'utf8', readFileHandler);
}

const readFileHandler = (error, data) => {
    if (error) {
        throw "unable to read input file";
    }

    fixExpenseReport(JSON.parse(data));
}

const fixExpenseReport = (entries) => {
    console.info("");
    console.info("Part two");

    const [one, two, three] = findThreeNumbersThatSumTo2020(entries);
    console.info("Entries that sum to 2020:", [one, two, three]);

    const result = one * two * three;
    console.info("Answer:", result);
}

const findThreeNumbersThatSumTo2020 = entries => {
    const numbers = []
    entries.some(a => 
        entries.some(b => 
            entries.some(c => {
                if (sumsTo2020(a, b, c)) {
                    numbers.push(a, b, c);
                    return true;
                }
            })
        )
    );
    return numbers;
}

const sumsTo2020 = (...numbers) => numbers.reduce(sum) == 2020;
const sum = (a, b) => a + b;

init();
