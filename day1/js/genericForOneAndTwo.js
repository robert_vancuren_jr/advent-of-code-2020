const fs = require('fs');
const { performance } = require('perf_hooks');

const init = () => {
    fs.readFile('entries.json', 'utf8', readFileHandler);
}

const readFileHandler = (error, data) => {
    if (error) {
        throw "unable to read input file";
    }

    fixExpenseReport(JSON.parse(data));
}

const fixExpenseReport = (entries) => {
    partOne(entries);
    partTwo(entries);
}

const partOne = (entries) => {
    console.info("");
    console.info("Part one");
    const [one, two] = findEm(2, entries);
    console.assert(one + two == 2020);
    console.info("Entries that sum to 2020:", [one, two]);

    const result = one * two;
    console.assert(result == 1018944);
    console.info("Answer:", result);
}

const partTwo = (entries) => {
    console.info("");
    console.info("Part two");
    const [one, two, three] = findEm(3, entries);
    console.assert(one + two + three == 2020);
    console.info("Entries that sum to 2020:", [one, two, three]);

    const result = one * two * three;
    console.assert(result == 8446464);
    console.info("Answer:", result);
}

const findEm = (numberOfEntriesToFind, entries) => {
  let generators = buildPermutationGenerators(numberOfEntriesToFind, entries);

  return findEntriesThatSumTo2020(generators);
}

const findEntriesThatSumTo2020 = generators => {
  while(true) {
    const nexts = generators.map((g) => g().next());
    if (nexts.some(x => x.done === true)) {
        break;
    }

    const values = nexts.map(x => x.value);
    if (valuesSumTo2020(values)) {
        return values;
    }
  }

  return []; 
}

const buildPermutationGenerators = (numberOfEntriesToFind, entries) => {
  let generators = [];
  for (let i = 0; i < numberOfEntriesToFind; i++) {
    const last = i != numberOfEntriesToFind - 1;
    generators.push(permutationGenerator(entries, i, last));
  }

  return generators;
}

const valuesSumTo2020 = (numbers) => numbers.reduce(sum) == 2020;
const sum = (a, b) => a + b;

/*
 * Given ([1, 2, 3]), 0)
 * this will yield 1, 2, 3, 1, 2, 3, 1, 2, 3... and never complete

 * Given ([1, 2, 3]), 0, false)
 * this will yield 1, 2, 3 and complete

 * Given ([1, 2, 3]), 2)
 * this will yield 1, 1, 1, 2, 2, 2, 3, 3, 3, 1, 1, 1, 2, 2, 2, 3, 3, 3... and never complete
 *
 */
const permutationGenerator = (array, permutationIndex, loop = true) => {
  if (permutationIndex != 0) {
    permutationIndex = Math.pow(array.length, permutationIndex);
  }
  let i = 0;
  let x = 0;

  return function* () {
    if (i >= array.length - 1 && x > permutationIndex) {
      i = 0;
    }

    if (x > permutationIndex) {
      x = 0;
      i += 1;
    }
    x += 1;

    yield array[i];
  };
};

init();
