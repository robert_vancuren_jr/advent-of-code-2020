open System

let partOne entries =
    printfn ""
    printfn "Part one"
    let entriesThatSumTo2020 = [
        for x in entries do
        for y in entries do
            if x + y = 2020 then yield x, y ]

    let (a, b) = entriesThatSumTo2020.Head 
    printfn "Entries that sum to 2020: %i %i" a b

    let answer = a * b
    printfn "Answer: %i" answer

let partTwo entries =
    printfn ""
    printfn "Part two"
    let entriesThatSumTo2020 = [
        for x in entries do
        for y in entries do
        for z in entries do
            if x + y + z = 2020 then yield x, y, z ]

    let (a, b, c) = entriesThatSumTo2020.Head 
    printfn "Entries that sum to 2020: %i %i %i" a b c

    let answer = a * b * c
    printfn "Answer: %i" answer

let loadEntries =
    let lines = System.IO.File.ReadLines("raw-input")
    [for x in lines do yield int x]

[<EntryPoint>]
let main argv =
    let entries = loadEntries

    partOne entries
    partTwo entries

    0 // return an integer exit code

    